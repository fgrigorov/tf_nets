#!bin/bash

echo "Building the trt porting..."
nvcc -std=c++14 -g -O infer.cc -o bin/infer -lnvinfer -lnvparsers
echo 'Completed the build!'
