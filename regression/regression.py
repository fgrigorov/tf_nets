import numpy as np
import tensorflow as tf

from tensorflow.python.tools import freeze_graph

import matplotlib.pyplot as plt

import os
import time

def generate_sinosoidal_data(a, b, w, limit):
	x = np.arange(0.0, limit, 0.8)
	y = a * np.cos(w * np.pi * x) + a * np.sin(w * np.pi * x) + b

	return np.expand_dims(x), np.expand_dims(y)

def generate_exp(n=200):
	#[200, 1]
	x_data = np.linspace(-0.5, 0.5, n)[:, np.newaxis]
	noise = np.random.normal(0, 0.02, x_data.shape)
	Y_data = np.square(x_data) + noise

	return x_data, Y_data

class Regressor:
	def __init__(self, mode='cpu', is_logging=False):
		config = tf.ConfigProto()
		config.log_device_placement = is_logging
		config.gpu_options.per_process_gpu_memory_fraction = 0.4
		self.sess = tf.Session(config=config)

		self.training_path = 'models/'

		self.device = tf.device('/cpu:0')
		if 'gpu' in mode:
			self.device = tf.device('/device:GPU:0')

		self.x = tf.placeholder(shape=[None, 1], dtype=np.float32)
		self.y_true = tf.placeholder(shape=[None, 1], dtype=np.float32)

		w1 = tf.Variable(np.random.normal(size=(1, 10)), dtype=np.float32, name='w1')
		b1 = tf.Variable(np.zeros(shape=(1, 10)), dtype=np.float32, name='b1')

		w2 = tf.Variable(np.random.normal(size=(10, 1)), dtype=np.float32, name='w2')
		b2 = tf.Variable(np.zeros(shape=(1, 1)), dtype=np.float32, name='b2')

		l1 = tf.nn.relu(tf.matmul(self.x, w1) + b1)
		self.pred = tf.nn.tanh(tf.matmul(l1, w2) + b2, name='tanh')

		self.cost = tf.reduce_mean(tf.square(self.pred - self.y_true))

		self.saver = tf.train.Saver()

	def __del__(self):
		if self.sess is not None:
			self.sess.close()
			print('Closed session!\n')

	def train(self, X, y_true, is_pretrained=True, epochs=2000, lr=0.2, prepare_to_freeze=False, is_freeze=False):
		print('Initiated training!\n')
		init = tf.global_variables_initializer()

		self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=lr).minimize(self.cost)

		assert(len(X) == len(y_true))

		if is_pretrained:
			self.saver.restore(self.sess, self.training_path + 'model.ckpt')
			print('Loaded pretrained model!\n')

		with self.device:
			self.sess.run(init)

			for epoch in range(1, epochs + 1):
				start_time = time.time()
				_, loss = self.sess.run((self.optimizer, self.cost), feed_dict={ self.x : X, self.y_true : y_true })
				end_time = time.time()
				elapsed = (end_time - start_time) * 1000.

				if epoch % 50 == 0:
					prediction = self.sess.run(self.pred, feed_dict={ self.x : X })
					print('epoch: {}  loss: {:0.5f}  time: {} ms\n'.format(epoch, loss, elapsed))

		print('Training is complete!\n')
		print('Saving model to \'models/model.ckpt\'\n')
		
		if not os.path.exists(self.training_path):
			os.mkdir(self.training_path)
		self.saver.save(self.sess, self.training_path + 'model.ckpt')

		if prepare_to_freeze:
			tf.train.write_graph(self.sess.graph.as_graph_def(), self.training_path, 'saved_model.pbtxt', as_text=True)
			print('Prepared the graph nodes for freeze!\n')

		if is_freeze:
			freeze_graph.freeze_graph( 	self.training_path + 'saved_model.pbtxt',
										'',
										False,
										self.training_path + 'model.ckpt',
										'tanh',
										'save/restore_all',
										'save/Const:0',
										self.training_path + 'saved_model.pb',
										True,
										'')

	def infer(self, X):
		start_time = time.time()
		pred = self.sess.run(self.pred, feed_dict={self.x : X})
		end_time = time.time()

		return pred

if __name__ == '__main__':
	a = 0.6
	b = 0
	w  = 0.3

	x_train, y_train = generate_exp(n=200)
	'''plt.title('Generated training data')
	plt.plot(x_train, y_train, 'r--')
	plt.ylim(-1, 1)
	plt.show()'''

	print('x_train shape: ', x_train.shape)
	print('y_train shape: ', y_train.shape)

	net = Regressor()
	net.train(x_train, y_train, is_pretrained=True, epochs=2000, lr=0.1, prepare_to_freeze=True, is_freeze=True)

	x_test, y_test = generate_exp(n=50)

	preds = net.infer(x_test)

	plt.title('Real versus predicted')
	plt.plot(x_test, y_test, 'r--')
	plt.plot(x_test, preds, 'b--')
	plt.ylim(-1, 1)
	plt.show()
