import argparse
import uff

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', help='Specify .pb input file location')
	parser.add_argument('-o', help='Specify .uff output file location')
	parsed_args = parser.parse_args()

	if parsed_args.i is None:
		raise('No input has been specified!\n')
	if parsed_args.o is None:
		raise('No output has been specified!\n')

	uff_model = uff.from_tensorflow_frozen_model(parsed_args.i, ["tanh"], output_filename=parsed_args.o)
