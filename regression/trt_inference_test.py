import argparse
import os
import tensorrt as trt
import uff

datatype = trt.infer.DataType.FLOAT

def infer(uff_file, plan_file):
	G_LOGGER = trt.infer.ConsoleLogger(trt.infer.LogSeverity.ERROR)
	builder = trt.infer.create_infer_builder(G_LOGGER)
	network = builder.create_network()
	uff_parser = trt.utils.uffparser.create_uff_parser()
	uff_parser.register_input('Placeholder', trt.infer.Dims2, trt.infer.Dims2)
	uff_parser.register_output('tanh')
	with open(uff_file, 'rb') as in_file:
		bytes = in_file.read()
	print(uff_parser.parse(bytes, network, datatype))

	uff_parser.destroy()

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', help='Specify input file')
	parser.add_argument('-o', help='Specify output argument')
	args = parser.parse_args()

	assert(args.i is not None)
	assert(args.o is not None)

	cwd = os.getcwd()
	uff_file = cwd + '/' + args.i
	plan_file = cwd + '/' + args.o
	print('UFF file: {}\n'.format(uff_file))
	print('Plan file": {}\n'.format(plan_file))

	infer(uff_file, plan_file)
