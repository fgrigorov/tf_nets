#include  <iostream>
#include <iterator>
#include <memory>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include <cuda_runtime_api.h>
#include <limits.h>
#include <stdio.h>
#include <unistd.h>

#include "NvInfer.h"
#include "NvUffParser.h"

class Logger : public nvinfer1::ILogger {
	void log(Severity severity, const char* msg) override {
		if (severity != Severity::kINFO) { std::cout << msg << std::endl; }
	}
};

class Builder : public nvinfer1::IBuilder {
public:
	static Builder* clone() { auto logger = Logger(); return reinterpret_cast<Builder*>(nvinfer1::createInferBuilder(logger)); }
};

class Network : public nvinfer1::INetworkDefinition {
public:
	static Network* clone(Builder *builder) { return reinterpret_cast<Network*>(builder->createNetwork()); }
};

class UffParser : public nvuffparser::IUffParser {
public:
	static UffParser* clone() { return reinterpret_cast<UffParser*>(nvuffparser::createUffParser()); }
};

class CudaEngine : public nvinfer1::ICudaEngine {
public:
	static CudaEngine* clone(Builder *builder, Network *network) { return reinterpret_cast<CudaEngine*>(builder->buildCudaEngine(*network)); }
};

class ExeContext : public nvinfer1::IExecutionContext {
public:
	static ExeContext* clone(CudaEngine *engine) { return reinterpret_cast<ExeContext*>(engine->createExecutionContext()); }
};

class Plan : public nvinfer1::IHostMemory {
public:
	static Plan* clone(CudaEngine *engine) { return reinterpret_cast<Plan*>(engine->serialize()); }
};

class Regressor {
public:
	Regressor() {
		//Note: Have to use .reset, as it accepts a raw pointer with new (can use an abstract class), std::make_unique unifies
		//the creation of the std::unique_ptr along with the new operator in one step but it uses perfect forwarding. It does not work.
		builder_.reset(Builder::clone());
		std::cout << "Created the builder object!\n";
		network_.reset(Network::clone(builder_.get()));
		std::cout << "Created the network object!\n";
		parser_.reset(UffParser::clone());
		std::cout << "Created the parser object!\n";

		builder_->setMaxBatchSize(1);
		builder_->setMaxWorkspaceSize(1<<30);//1Gb of reserved memory
		std::cout << "Set builder\'s options!\n";
	}
	~Regressor() = default;

	void parse(const std::string &uff_file_path, const std::string &plan_file_path,  int batch_size, bool is_serialized = false) {
		char cwd_buff[PATH_MAX];
		auto res = getcwd(cwd_buff, sizeof(cwd_buff));
		if (!res) { throw std::runtime_error("Could not get current working directory!\n"); }
		std::string cwd{ cwd_buff };
		cwd += "/";

		if (network_ && parser_) {
			std::cout << "Ready to parse the specified uff @ " << uff_file_path << "\n";
			parser_->registerInput("Placeholder", nvinfer1::Dims2, nvinfer1::Dims);
			parser_->registerOutput("tanh");
			std::fstream in_file(uff_file_path, std::ios_base::in | std::ios_base::binary);
			if (in_file.is_open()) {
				std::string full_path = cwd + uff_file_path;
				std::cout << full_path << std::endl;
				if (!parser_->parse(full_path.c_str(), *network_)) { 
					throw std::runtime_error("Could not parse the file!\n"); 
				}
				in_file.close();
				std::cout << "Parsed the uff file!\n";
			}
		}
		engine_.reset(CudaEngine::clone(builder_.get(), network_.get()));
		std::cout << "Created the cuda engine object!\n";

		if (is_serialized) {
			plan_.reset(Plan::clone(engine_.get()));
			std::cout << "Created the IHostMemory object!\n";
			std::string full_path = cwd + plan_file_path + "saved_plan.plan";
			std::fstream out_file(full_path, std::ios_base::out | std::ios_base::binary);
			if (out_file.is_open()) { 
				out_file.write(reinterpret_cast<const char*>(plan_->data()), plan_->size());
				out_file.close();
				std::cout << "Saved the plan file!\n";
			}
		}

		if (!exe_context_) {
			exe_context_.reset(ExeContext::clone(engine_.get()));
			std::cout << "Created the executable context object to run the gpu kernels!\n";
		}

		//TODO (create irun context for the gpu kernels)
	}

	//TODO
	void load(const std::string &plan_file) {
		//Create IRuntime
		//Deserialize
	}

	//TODO
	template <typename T>
	std::vector<T> infer(const std::vector<T> &inputs){
		return std::vector<T>();
	}

private:
	unsigned ElementSize(nvinfer1::DataType type) {
		if (type == nvinfer1::DataType::kFLOAT)			return 4;
		else if (type == nvinfer1::DataType::kHALF) 	return 2;
		else if (type == nvinfer1::DataType::kINT8)		return 1;

		return 0;
	}

	std::unique_ptr<Builder> builder_ = nullptr;
	std::unique_ptr<Network> network_ = nullptr;
	std::unique_ptr<UffParser> parser_ = nullptr;
	std::unique_ptr<CudaEngine> engine_ = nullptr;
	std::unique_ptr<ExeContext> exe_context_ = nullptr;
	std::unique_ptr<Plan> plan_ = nullptr;
};

int main(int argc, char** argv) {
	if (argc < 3) { throw std::runtime_error("An output directory has not been specified!"); }

	std::string uff_dir{argv[1]};
	std::string plan_dir{argv[2]};

	std::cout << "Uff file is at " << uff_dir << "\n";
	std::cout << "Plan file is at " << plan_dir << std::endl;

	std::unique_ptr<Regressor> inferer = std::make_unique<Regressor>();
	inferer->parse(uff_dir, plan_dir, true);
}  // namespace
